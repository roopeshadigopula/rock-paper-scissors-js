function rpsGame(yourChoice) {
  declareResult(yourChoice.id, botOption());
}

function botOption() {
  var bot = Math.floor(Math.random() * 3);
  return ["rock", "paper", "scissors"][bot];
}

function declareResult(human, bot) {
  var analyze_human_result = {
    rock: { scissors: "1", rock: "0.5", paper: "0" },
    paper: { scissors: "0", rock: "1", paper: "0.5" },
    scissors: { scissors: "0.5", rock: "0", paper: "1" },
  };

  var humanScore = analyze_human_result[human][bot];
  console.log("My Choice: " + human);
  console.log("Bot Choice: " + bot);
  console.log(humanScore);
  console.log(finalMessage(humanScore));

  frontEnd(human, bot, finalMessage(humanScore));
}

function finalMessage(humanScore) {
  if (humanScore === "0") return { message: "You Lost!", color: "red" };
  else if (humanScore === "0.5") return { message: "~Draw~", color: "yellow" };
  else if (humanScore === "1") return { message: "You Won!", color: "green" };
}

function frontEnd(humanImage, botImage, finalMessage) {
  var imageDB = {
    rock: document.getElementById("rock").src,
    paper: document.getElementById("paper").src,
    scissors: document.getElementById("scissors").src,
  };

  var humanDiv = document.createElement("div");
  var botDiv = document.createElement("div");
  var messageDiv = document.createElement("div");

  humanDiv.innerHTML =
    "<img src='" +
    imageDB[humanImage] +
    "' style='box-shadow: 0px, 10px, 50px, rgba(37, 50, 233, 1);'>";
  botDiv.innerHTML =
    "<img src='" +
    imageDB[botImage] +
    "' style='box-shadow: 0px, 10px, 50px, rgba(243, 30, 24, 1);'>";
  messageDiv.innerHTML =
    "<h1 style='color: " +
    finalMessage["color"] +
    "; font-size: 60px; padding:30px;'>" +
    finalMessage["message"] +
    "</h1>";

  document.getElementById("result__image__canvas__div").appendChild(humanDiv);
  document.getElementById("result__image__canvas__div").appendChild(messageDiv);
  document.getElementById("result__image__canvas__div").appendChild(botDiv);

  //   clearBox();
}

function clearBox() {
  document.getElementById("result__image__canvas__div").innerHTML = "";
}
